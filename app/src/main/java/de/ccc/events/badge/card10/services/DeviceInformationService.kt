package de.ccc.events.badge.card10.services

import android.bluetooth.BluetoothGattCharacteristic
import android.bluetooth.BluetoothGattService
import android.util.Log
import de.ccc.events.badge.card10.*
import de.ccc.events.badge.card10.common.ConnectionService
import java.nio.ByteBuffer

private const val TAG = "DeviceInformationService"

class DeviceInformationService(
    service: BluetoothGattService
) {
    private val firmwareRevision = service.getCharacteristic(FIMRWARE_REVISION_CHARACTERISTIC_UUID)


    fun getFirmwareRevision() {
        ConnectionService.readCharacteristic(firmwareRevision)
    }
}