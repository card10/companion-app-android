/*
 * Copyright by the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.ccc.events.badge.card10.scanner

import android.bluetooth.BluetoothDevice
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import de.ccc.events.badge.card10.R

class DeviceViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
    RecyclerView.ViewHolder(inflater.inflate(R.layout.scanner_list_item, parent, false)) {

    private var nameView: TextView? = null
    private var btMacView: TextView? = null
    private var statusView: TextView? = null

    init {
        nameView = itemView.findViewById(R.id.scanner_list_item_name)
        btMacView = itemView.findViewById(R.id.scanner_list_item_btmac)
        statusView = itemView.findViewById(R.id.scanner_list_item_status)
    }

    fun bind(device: Device, clickListener: (Device) -> Unit) {
        nameView?.text = device.name
        btMacView?.text = device.btMac
        val typeface = if (device.state == BluetoothDevice.BOND_BONDED) Typeface.DEFAULT_BOLD else Typeface.DEFAULT
        btMacView?.typeface = typeface
        statusView?.text = if (device.state == BluetoothDevice.BOND_BONDING) "BONDING..." else if (device.state == BluetoothDevice.BOND_BONDED) "BONDED" else "NOT BONDED"
        statusView?.setTextColor( if (device.state == BluetoothDevice.BOND_BONDED) ContextCompat.getColor(itemView.context, R.color.colorStatusConnected) else ContextCompat.getColor(itemView.context, R.color.colorStatusDisconnected))
        btMacView?.typeface = typeface
        itemView.setOnClickListener {
            clickListener(device)
        }
    }
}
