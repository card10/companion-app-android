/*
 * Copyright by the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.ccc.events.badge.card10.common

import android.bluetooth.*
import android.content.Context
import android.util.Log
import de.ccc.events.badge.card10.CARD10_BLUETOOTH_MAC_PREFIX
import de.ccc.events.badge.card10.CARD10_SERVICE_UUID
import de.ccc.events.badge.card10.FILE_SERVICE_UUID
import de.ccc.events.badge.card10.DEVICE_INFORMATION_SERVICE_UUID
import de.ccc.events.badge.card10.CARD10_ECG_SERVICE_UUID
import de.ccc.events.badge.card10.R
import de.ccc.events.badge.card10.filetransfer.LowEffortService
import de.ccc.events.badge.card10.services.Card10EcgService
import de.ccc.events.badge.card10.services.Card10Service
import de.ccc.events.badge.card10.services.DeviceInformationService

private const val TAG = "ConnectionService"

object ConnectionService {
    var device: BluetoothDevice? = null
    var leService: LowEffortService? = null
    var card10Service: Card10Service? = null
    var card10EcgService: Card10EcgService? = null
    var diService: DeviceInformationService? = null
    var mtu = 20

    private var connection: BluetoothGatt? = null

    private var gattListeners = mutableMapOf<String, GattListener>()

    val deviceName: String?
        get() = device?.name

    val deviceAddress: String?
        get() = device?.address

    fun hasDevice(): Boolean {
        return device != null
    }

    fun isPaired(): Boolean {
        val bondedDevices = BluetoothAdapter.getDefaultAdapter().bondedDevices.filter {
            it.address.startsWith(
                CARD10_BLUETOOTH_MAC_PREFIX,
                true
            )
        }

        return bondedDevices.isNotEmpty()
    }

    fun addGattListener(tag: String, listener: GattListener) {
        gattListeners[tag] = listener
    }

    // Looks for bonded devices and gets one to use. Returns true if successful.
    fun grabBondedDevice() : Boolean {
        // don't grab a new device if we already have a connection
        if(connection != null) {
            return true
        }

        // Use first BLE devices that is bonded
        val bondedDevices = BluetoothAdapter.getDefaultAdapter().bondedDevices.filter {
            it.address.startsWith(
                    CARD10_BLUETOOTH_MAC_PREFIX,
                    true
            )
        }

        if (bondedDevices.isEmpty()) {
            return false
        }

        device = bondedDevices[0]

        return true
    }

    fun connect(context: Context) {

        if (! hasDevice()) {
            if(! grabBondedDevice()) {
                throw ConnectionException(context.getString(R.string.connection_error_no_bonded_device))
            }
        }

        // 1. Connect
        // 2. Refresh GATT database
        // 3. Discover services
        // 4. Change MTU
        // 5. ???
        // 6. Profit
        connection = device?.connectGatt(context, true, gattCallback)
    }

    fun disconnect() {
        connection?.disconnect()

        // TODO: Maybe we can keep the connection around instead of creating a new one every time?
        connection?.close()
        connection = null
    }

    private val gattCallback = object : BluetoothGattCallback() {
        override fun onServicesDiscovered(gatt: BluetoothGatt?, status: Int) {
            if (gatt == null) {
                throw NullPointerException()
            }

            connection = gatt
            card10EcgService = null

            for (service in gatt.services) {
                Log.d(TAG, "Found service: ${service.uuid}")

                if (service.uuid == FILE_SERVICE_UUID) {
                    leService = LowEffortService(service)
                } else if (service.uuid == CARD10_SERVICE_UUID) {
                    card10Service = Card10Service(service)
                } else if (service.uuid == CARD10_ECG_SERVICE_UUID) {
                    card10EcgService = Card10EcgService(service, connection)
                } else if(service.uuid == DEVICE_INFORMATION_SERVICE_UUID) {
                    diService = DeviceInformationService(service)
                }
            }

            if (leService == null) {
                Log.e(TAG, "Could not find file transfer service")
                return
            }

            gatt.requestMtu(100)
        }

        override fun onConnectionStateChange(gatt: BluetoothGatt?, status: Int, newState: Int) {
            connection = gatt

            when (newState) {
                BluetoothGatt.STATE_CONNECTED -> {
                    // Force a refresh of the GATT database. Somehow Android does not respect our
                    // "Generic Attribute" service with its "Service Changed" attribute
                    gatt?.javaClass?.getMethod("refresh")?.invoke(gatt)
                    gatt?.discoverServices()
                }
                BluetoothGatt.STATE_DISCONNECTED -> {
                    gattListeners.values.map { it.onConnectionLost() }
                    card10EcgService = null
                }
            }
        }

        override fun onMtuChanged(gatt: BluetoothGatt?, newMtu: Int, status: Int) {
            Log.d(TAG, "MTU changed to: $newMtu")

            checkNotNull(gatt)

            mtu = newMtu - 3 // Very precise science

            leService?.enableNotify(gatt)

            gattListeners.values.map { it.onConnectionReady() }
        }

        override fun onCharacteristicWrite(
            gatt: BluetoothGatt?,
            characteristic: BluetoothGattCharacteristic?,
            status: Int
        ) {
            checkNotNull(gatt)
            checkNotNull(characteristic)
            connection = gatt

            gattListeners.values.map { it.onCharacteristicWrite(characteristic, status) }
        }

        override fun onCharacteristicChanged(gatt: BluetoothGatt?, characteristic: BluetoothGattCharacteristic?) {
            connection = gatt

            checkNotNull(gatt)
            checkNotNull(characteristic)

            gattListeners.values.map { it.onCharacteristicChanged(characteristic) }
        }

        override fun onCharacteristicRead(gatt: BluetoothGatt?, characteristic: BluetoothGattCharacteristic ,
                                          status: Int ) {
            gattListeners.values.map { it.onCharacteristicRead(characteristic, status) }
        }
    }

    fun writeCharacteristic(characteristic: BluetoothGattCharacteristic): Boolean {
        val status = connection?.writeCharacteristic(characteristic) ?: false

        if (!status) {
            Log.d(TAG, "Write status: $status")
        }

        return status
    }

    fun readCharacteristic(characteristic: BluetoothGattCharacteristic): Boolean {
        val status = connection?.readCharacteristic(characteristic)?: false

        if (!status) {
            Log.w(TAG, "Read status of ${characteristic.uuid}: $status")
        }

        return status
    }

}