package de.ccc.events.badge.card10.common

import de.ccc.events.badge.card10.common.ConnectionState

interface ConnectionStateListener {
    fun onConnectionStateChanged(newState : ConnectionState)
}