package de.ccc.events.badge.card10.background

import android.app.*
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Binder
import android.os.Build
import android.os.IBinder
import android.widget.Toast
import androidx.core.app.NotificationCompat
import de.ccc.events.badge.card10.MainActivity
import de.ccc.events.badge.card10.R
import de.ccc.events.badge.card10.common.ConnectionException
import de.ccc.events.badge.card10.common.ConnectionService
import de.ccc.events.badge.card10.common.ConnectionState
import de.ccc.events.badge.card10.common.GattListener
import de.ccc.events.badge.card10.time.CurrentTimeServer

private const val NOTIFICATION_ID = 1337
private const val CHANNEL_ID = "Card10BackgroundService"

class Card10BackgroundService : Service(), GattListener {
    lateinit var connectionService : ConnectionService
    private var state
        get() = ConnectionState.state
        set(value) {
            ConnectionState.state = value
        }

    fun toast(text : String) {
        val duration = Toast.LENGTH_SHORT

        val toast = Toast.makeText(applicationContext, text, duration)
        toast.show()
    }

    private val bluetoothReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val btState = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.STATE_OFF)

            when (btState) {
                BluetoothAdapter.STATE_ON -> {
                    checkPairing()
                }
                BluetoothAdapter.STATE_OFF -> {
                    deactivate()
                    state = ConnectionState.NO_BLUETOOTH;
                }
            }
        }
    }

    inner class LocalBinder : Binder() {
        val service: Card10BackgroundService = this@Card10BackgroundService
    }

    override fun onBind(intent: Intent): IBinder {
        return LocalBinder()
    }

    override fun onCreate() {
        super.onCreate()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create the NotificationChannel
            val name = "Background Service"
            val descriptionText = "for the background service"
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val mChannel = NotificationChannel(CHANNEL_ID, name, importance)
            mChannel.description = descriptionText
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(mChannel)
        }

        connectionService = ConnectionService

        val bluetoothEventFilter = IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED)
        registerReceiver(bluetoothReceiver, bluetoothEventFilter)

        checkBluetooth()
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    // transitions from *everything*  to NO_BLUETOOTH
    // transitions from NO_BLUETOOTH  to NOT_CONNECTED
    // transitions from NO_BLUETOOTH  to NOT_PAIRED
    fun checkBluetooth() {
        if(! BluetoothAdapter.getDefaultAdapter().isEnabled) {
            deactivate()
            state = ConnectionState.NO_BLUETOOTH
        } else if (state == ConnectionState.NO_BLUETOOTH || state == ConnectionState.NOT_CONNECTED || state == ConnectionState.NOT_PAIRED) {
            // Maybe just an else would also be good enough...
            checkPairing()
        }
    }

    // transitions from NO_BLUETOOTH  to NOT_CONNECTED
    // transitions from NO_BLUETOOTH  to NOT_PAIRED
    // transitions from NOT_PAIRED    to NOT_CONNECTED
    // transitions from NOT_CONNECTED to NOT_PAIRED
    // transitions from CONNECTING    to NOT_PAIRED
    // transitions from CONNECTED     to NOT_PAIRED
    fun checkPairing() {
        if(! ConnectionService.grabBondedDevice()) {
            deactivate()
            state = ConnectionState.NOT_PAIRED
        } else if (state == ConnectionState.NOT_PAIRED || state == ConnectionState.NO_BLUETOOTH) {
            state = ConnectionState.NOT_CONNECTED
        }
    }

    fun isActive() : Boolean {
        return state == ConnectionState.CONNECTING || state == ConnectionState.CONNECTED
    }

    // transitions from NOT_CONNECTED to CONNECTING
    // transitions from CONNECTING    to NOT_CONNECTED
    // transitions from CONNECTED     to NOT_CONNECTED
    fun toggleActive() {
        if(state == ConnectionState.NOT_CONNECTED) {
            activate()
        } else if (isActive()) {
            deactivate()
        }
    }

    // transitions from NOT_CONNECTED to CONNECTING
    fun activate() {
        if(state == ConnectionState.NOT_CONNECTED) {
            try {
                connectionService.connect(applicationContext)
                state = ConnectionState.CONNECTING
                connectionService.addGattListener("bg", this)

                val bluetoothManager = getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
                CurrentTimeServer.registerReceivers(this)
                CurrentTimeServer.startServer(applicationContext, bluetoothManager)

                Intent(this, Card10BackgroundService::class.java).also { intent ->
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        startForegroundService(intent)
                    } else {
                        startService(intent)
                    }
                }

                foreground()
            } catch (e: ConnectionException) {
                toast("no card10 paired!")
            }
        }
    }

    // transitions from CONNECTING to NOT_CONNECTED
    // transitions from CONNECTED  to NOT_CONNECTED
    fun deactivate() {
        if(isActive()) {
            state = ConnectionState.NOT_CONNECTED
            connectionService.disconnect()

            CurrentTimeServer.unregisterReceivers(this)
            CurrentTimeServer.stopServer()

            stopForeground(true)
            stopSelf()
        }
    }

    private fun foreground(connected: Boolean = false) {

        val pendingIntent: PendingIntent =
            Intent(this, MainActivity::class.java).let { notificationIntent ->
                PendingIntent.getActivity(this, 0, notificationIntent, 0)
            }

        val builder = NotificationCompat.Builder(this, CHANNEL_ID)
            .setPriority(NotificationCompat.PRIORITY_LOW)
            .setOngoing(true)
            .setShowWhen(true)
            .setContentIntent(pendingIntent)

        val notification = if (connected) {
            builder.setContentTitle(getString(R.string.notification_connected_title))
                    .setContentText(getString(R.string.notification_connected_text))
                    .setSmallIcon(R.drawable.ic_notification)
                    .setOnlyAlertOnce(true)
                    .build()
        } else {
            builder.setContentTitle(getString(R.string.notification_disconnected_title))
                    .setContentText(getString(R.string.notification_disconnected_text))
                    .setSmallIcon(R.drawable.ic_notification_disconnected)
                    .setOnlyAlertOnce(true)
                    .build()
        }

        startForeground(NOTIFICATION_ID, notification)
    }

    // transitions from CONNECTING to CONNECTED
    override fun onConnectionReady() {
        if(state == ConnectionState.CONNECTING) {
            state = ConnectionState.CONNECTED
            foreground(true)
        }
    }

    // transitions from CONNECTED to CONNECTING
    override fun onConnectionLost() {
        if(state == ConnectionState.CONNECTED) {
            state = ConnectionState.CONNECTING
            foreground(false)
        }
    }

}