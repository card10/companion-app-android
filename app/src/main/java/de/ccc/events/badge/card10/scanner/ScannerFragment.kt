/*
 * Copyright by the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.ccc.events.badge.card10.scanner

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.le.ScanCallback
import android.bluetooth.le.ScanResult
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.transition.Fade
import de.ccc.events.badge.card10.CARD10_BLUETOOTH_MAC_PREFIX
import kotlinx.android.synthetic.main.scanner_fragment.*
import androidx.transition.TransitionManager
import de.ccc.events.badge.card10.R

class ScannerFragment : Fragment() {

    lateinit var bluetoothAdapter: BluetoothAdapter
    val listAdapter = ScannerListAdapter({ device: Device -> deviceClicked(device) })
    val viewModel: ScannerViewModel by lazy { ViewModelProviders.of(this).get(ScannerViewModel::class.java) }

    val callback = object : ScanCallback() {
        override fun onScanResult(callbackType: Int, result: ScanResult) {
            val device = result.device
            if (device.address.startsWith(CARD10_BLUETOOTH_MAC_PREFIX, true))
                upsertToListAdapter(device)
        }
    }

    private fun upsertToListAdapter(device: BluetoothDevice) {
        if (listAdapter.itemCount == 0) {
            viewModel.showModal.value = false
        }
        listAdapter.upsert(
            Device(
                btMac = device.address,
                name = device.name,
                state = device.bondState
            )
        )
    }

    // TODO: Remove devices if we lose their connection.
    val receiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val device = intent.extras!!.getParcelable(BluetoothDevice.EXTRA_DEVICE) as BluetoothDevice?
                ?: return
            val status =
                if (device.bondState == BluetoothDevice.BOND_NONE) "NONE" else if (device.bondState == BluetoothDevice.BOND_BONDING) "BONDING" else if (device.bondState == BluetoothDevice.BOND_BONDED) "BONDED" else "NULL"
            System.out.println("=== onReceive ${intent.action} ${status} ${device}")
            upsertToListAdapter(device)
            listAdapter.notifyDataSetChanged()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel.modalMessage.observe(
            this,
            Observer { text -> scanner_modal.text = text })
        viewModel.showModal.observe(this, Observer { show ->
            val transition = Fade()
            transition.duration = 1000
            transition.targets.add(scanner_modal_scrim)
            TransitionManager.beginDelayedTransition(scanner_device_list, transition)
            scanner_modal_scrim.visibility = if (show) View.VISIBLE else View.GONE
            scanner_modal_scrim.isClickable = show
            scanner_modal_scrim.isFocusable = show
        })
        activity?.registerReceiver(receiver, IntentFilter(BluetoothDevice.ACTION_BOND_STATE_CHANGED))
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
        val devices = bluetoothAdapter.bondedDevices.filter {
            it.address.startsWith(
                CARD10_BLUETOOTH_MAC_PREFIX,
                true
            )
        }
        for (device in devices) {
            upsertToListAdapter(device)
        }
        // TODO: If we the ability to remove devices, show this modal if the list is empty.
        if (devices.isEmpty()) {
            viewModel.modalMessage.value = context?.resources?.getString(R.string.scan_no_devices)
            viewModel.showModal.value = true
        }
        bluetoothAdapter.bluetoothLeScanner.startScan(callback)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.scanner_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        scanner_device_list.apply {
            layoutManager = LinearLayoutManager(activity)
            adapter = listAdapter
        }
    }

    override fun onDestroy() {
        bluetoothAdapter.bluetoothLeScanner.stopScan(callback)
        activity?.unregisterReceiver(receiver)
        super.onDestroy()
    }

    fun deviceClicked(device: Device) {
        System.out.println("===== deviceClicked " + device)
        bluetoothAdapter.bluetoothLeScanner.stopScan(callback)
        val bluetoothDevice = bluetoothAdapter.getRemoteDevice(device.btMac)
        if (bluetoothDevice.bondState == BluetoothDevice.BOND_NONE) {
            System.out.println("= creating bond " + device)
            bluetoothDevice.createBond()
        } else if (bluetoothDevice.bondState == BluetoothDevice.BOND_BONDED) {
            System.out.println("= removing bond " + device)
            removeBond(bluetoothDevice)
        }
    }

    fun removeBond(device: BluetoothDevice) {
        device.javaClass.getMethod("removeBond").invoke(device)
    }
}
