package de.ccc.events.badge.card10.ecg

import android.graphics.Paint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.androidplot.util.PixelUtils
import com.androidplot.util.Redrawer
import com.androidplot.xy.*
import de.ccc.events.badge.card10.R
import de.ccc.events.badge.card10.common.ConnectionService
import de.ccc.events.badge.card10.services.Card10Service
import kotlinx.android.synthetic.main.ecg_fragment.*
import java.lang.ref.WeakReference
import java.nio.ByteBuffer

private const val TAG = "EcgFragment"

class EcgFragment: Fragment() {
    private var card10Service: Card10Service? = null
    private var redrawer: Redrawer? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        card10Service = ConnectionService.card10Service

        PixelUtils.init(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.ecg_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val format = MyFadeFormatter(512)
        format.linePaint.strokeWidth = 6F

        val ecgSeries = ECGModel(512)
        plot.addSeries(ecgSeries, format)

        plot.setDomainBoundaries(0, 512, BoundaryMode.AUTO)

        // Reduce the number of range labels
        plot.linesPerRangeLabel = 3

        val renderer = plot.getRenderer(
            AdvancedLineAndPointRenderer::class.java
        )

        ecgSeries.start(
            WeakReference<AdvancedLineAndPointRenderer>(
                renderer
            )
        )

        // Set a redraw rate of 30 Hz and start immediately
        redrawer = Redrawer(plot, 30f, true)

        ConnectionService.card10EcgService?.enableNotify()
        ConnectionService.card10EcgService?.setListener(ecgSeries)
    }

    override fun onDestroy() {
        super.onDestroy()

        ConnectionService.card10EcgService?.disableNotify()
        ConnectionService.card10EcgService?.setListener(null)
        redrawer?.finish()
    }

    /**
     * Special [AdvancedLineAndPointRenderer.Formatter] that draws a line
     * that fades over time.  Designed to be used in conjunction with a circular buffer model.
     */
    class MyFadeFormatter internal constructor(private val trailSize: Int) :
        AdvancedLineAndPointRenderer.Formatter() {
        override fun getLinePaint(thisIndex: Int, latestIndex: Int, seriesSize: Int): Paint {
            // offset from the latest index:
            val offset: Int
            offset = if (thisIndex > latestIndex) {
                latestIndex + (seriesSize - thisIndex)
            } else {
                latestIndex - thisIndex
            }
            val scale = 255f / trailSize
            val alpha = (255 - offset * scale).toInt()
            linePaint.alpha = if (alpha > 0) alpha else 0
            return linePaint
        }
    }
}

class ECGModel(size: Int) : XYSeries {
    private val data: Array<Number?>?
    private var lastIndex = 0
    private lateinit var rendererRef: WeakReference<AdvancedLineAndPointRenderer>
    fun start(rendererRef: WeakReference<AdvancedLineAndPointRenderer>) {
        this.rendererRef = rendererRef
    }

    override fun size(): Int {
        return data!!.size
    }

    override fun getX(index: Int): Number? {
        return index/128.0
    }

    override fun getY(index: Int): Number? {
        return data!![index]
    }

    override fun getTitle(): String? {
        return "Voltage"
    }

    fun onEcgDataReceived(ecgData: ByteArray) {
        for(i in 2 until ecgData.size step 2) {
            val value = ByteBuffer.wrap(ecgData.copyOfRange(i,i+2)).short
            data?.set(lastIndex, value)
            lastIndex += 1
            if(lastIndex == data!!.size) lastIndex = 0
        }
        rendererRef?.get()?.setLatestIndex(lastIndex)
    }

    init {
        data = arrayOfNulls<Number?>(size)
        for (i in data.indices) {
            data[i] = 0
        }
    }
}