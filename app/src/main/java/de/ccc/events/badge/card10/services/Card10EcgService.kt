/*
 * Copyright by the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.ccc.events.badge.card10.services


import android.bluetooth.BluetoothGatt
import android.bluetooth.BluetoothGattCharacteristic
import android.bluetooth.BluetoothGattDescriptor
import android.bluetooth.BluetoothGattService
import android.util.Log
import de.ccc.events.badge.card10.ECG_SAMPLES_CHARACTERISTIC_UUID
import de.ccc.events.badge.card10.common.ConnectionService
import de.ccc.events.badge.card10.common.GattListener
import de.ccc.events.badge.card10.ecg.ECGModel
import java.util.*


private const val TAG = "Card10EcgService"

class Card10EcgService(
    service: BluetoothGattService,
    connection: BluetoothGatt?
) : GattListener {
    private val ecgRx: BluetoothGattCharacteristic

    private var notifyEnabled = false
    private var listener: ECGModel? = null
    private var connection: BluetoothGatt? = null

    init {
        val rx = service.getCharacteristic(ECG_SAMPLES_CHARACTERISTIC_UUID)

        if (rx == null) {
            throw IllegalStateException()
        }

        this.connection = connection
        ecgRx = rx

        ConnectionService.addGattListener(TAG,this)
    }

    private fun setNotify(enable: Boolean)
    {
        val CHARACTERISTIC_UPDATE_NOTIFICATION_DESCRIPTOR_UUID: UUID = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");


        val cccd: BluetoothGattDescriptor =
            ecgRx.getDescriptor(CHARACTERISTIC_UPDATE_NOTIFICATION_DESCRIPTOR_UUID)
        if (enable) {
            cccd.value = BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE
        } else {
            cccd.value = BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE
        }
        val writeSuccess = connection!!.writeDescriptor(cccd)
        Log.d(TAG, "cccd write: $writeSuccess")

        val notifySuccess = connection!!.setCharacteristicNotification(ecgRx, enable)
        Log.d(TAG, "notify change: $notifySuccess")

        if (!notifySuccess) {
            Log.e(TAG, "Notify change failed")
        } else {
            notifyEnabled = enable
        }
    }

    fun enableNotify() {
        setNotify(true)
    }

    fun disableNotify() {
        setNotify(false)
    }

    fun setListener(ecgModel: ECGModel?)
    {
        listener = ecgModel
    }

    override fun onCharacteristicChanged(characteristic: BluetoothGattCharacteristic) {
        if (characteristic.uuid != ECG_SAMPLES_CHARACTERISTIC_UUID) {
            return
        }

        //fun ByteArray.toHex(): String = joinToString(separator = "") { eachByte -> "%02x".format(eachByte) }
        //Log.d(TAG, characteristic.value.toHex())

        listener?.onEcgDataReceived(characteristic.value)
    }
}