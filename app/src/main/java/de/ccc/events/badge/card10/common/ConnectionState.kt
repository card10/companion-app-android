package de.ccc.events.badge.card10.common

/*
 * Represents and saves app-wide connection state.
 *
 * Card10BackgroundService is responsible for maintaining state.
 *
 * The visible (resumed) fragment may listen to state changes. Currently, only MainFragment makes
 * use of this.
 */

enum class ConnectionState {
    NO_BLUETOOTH,
    NOT_PAIRED,
    NOT_CONNECTED,
    CONNECTING,
    CONNECTED;

    companion object {
        var listener: ConnectionStateListener? = null
            get() = field
            set(value) {
                field = value
                value?.onConnectionStateChanged(state)
            }
        var state = NO_BLUETOOTH
            get() = field
            set(value) {
                field = value
                listener?.onConnectionStateChanged(value)
            }
    }
}