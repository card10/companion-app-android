/*
 * Copyright by the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.ccc.events.badge.card10.filetransfer

import android.content.Context
import android.net.Uri
import android.util.Log
import de.ccc.events.badge.card10.filetransfer.protocol.Packet
import de.ccc.events.badge.card10.filetransfer.protocol.PacketType
import java.lang.IllegalStateException
import java.nio.ByteBuffer

private const val TAG = "ChunkedReader"

class ChunkedReader(
    context: Context,
    uri: Uri,
    private val mtu: Int
) {
    private val iterator: Iterator<Byte>
    private var lastPacket: Packet? = null

    init {
        val inputStream = context.contentResolver.openInputStream(uri) ?: throw IllegalStateException()
        val bytes = inputStream.readBytes()
        iterator = bytes.iterator()
    }

    private var offset: Int = 0
    private var lastCrc: Long = 0

    fun verifyCrc(crc: Long): Boolean {
        Log.d(TAG, "lastCrc: $lastCrc, crc: $crc")
        Log.d(TAG, "lastCrc: ${java.lang.Long.toHexString(lastCrc)}, crc: ${java.lang.Long.toHexString(crc)}")
        return (lastCrc == crc)
    }

    fun isDone() = !iterator.hasNext()

    fun getLast(): Packet {
        return lastPacket ?: throw IllegalStateException()
    }

    fun getNext(): Packet {
        Log.d(TAG, "Next chunk: $offset")

        if (!iterator.hasNext()) {
            throw IndexOutOfBoundsException()
        }

        // Our packet:
        // 1 byte header + 4 byte offset + chunk
        val maxChunkSize = mtu - 5

        val buffer = ByteBuffer.allocate(mtu - 1)
        buffer.putInt(offset)

        // TODO: Make less hacky
        var n = 0
        for (i in 0 until maxChunkSize) {
            if (!iterator.hasNext()) {
                break
            }

            buffer.put(iterator.next())
            n++
        }

        val byteArray = buffer.array().sliceArray(0 until n + 4)
        offset += n

        Log.d(TAG, "n: $n offset: $offset")

        val packet = Packet(
            PacketType.CHUNK,
            byteArray
        )

        lastCrc = packet.getCrc()

        lastPacket = packet

        return packet
    }
}