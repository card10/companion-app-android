/*
 * Copyright by the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.ccc.events.badge.card10.main

import android.app.AlertDialog
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothGattCharacteristic
import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import de.ccc.events.badge.card10.FIMRWARE_REVISION_CHARACTERISTIC_UUID
import de.ccc.events.badge.card10.MainActivity
import de.ccc.events.badge.card10.common.ConnectionService
import de.ccc.events.badge.card10.common.ConnectionState
import de.ccc.events.badge.card10.common.ConnectionStateListener
import de.ccc.events.badge.card10.common.GattListener
import de.ccc.events.badge.card10.time.TimeUpdateDialog
import de.ccc.events.badge.card10.R
import kotlinx.android.synthetic.main.main_fragment.*


class MainFragment : Fragment(), ConnectionStateListener, GattListener {
    private val bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
    private var firmwareVersion: String? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if(activity!!.intent.action == "application/x.card10.app") {
            findNavController().navigate(MainFragmentDirections.Installer())
            return null
        }
        return inflater.inflate(R.layout.main_fragment, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val con = findNavController()

        button_pair.setOnClickListener { con.navigate(MainFragmentDirections.Scan()) }
        button_mood.setOnClickListener { con.navigate(MainFragmentDirections.Mood()) }
        button_beautiful.setOnClickListener { con.navigate(MainFragmentDirections.Beautiful()) }
        button_ecg.setOnClickListener { con.navigate(MainFragmentDirections.Ecg()) }
        button_hatchery.setOnClickListener { con.navigate(MainFragmentDirections.AppList()) }
        button_send.setOnClickListener { con.navigate(MainFragmentDirections.Transfer()) }
        button_connect.setOnClickListener {
            (activity as? MainActivity)?.card10BackgroundService?.toggleActive()
        }

        button_set_time.setOnClickListener {
            val dialogFragment = TimeUpdateDialog()
            dialogFragment.show(fragmentManager!!, "time")
            dialogFragment.setTime()
            dialogFragment.dismiss()
        }
    }

    override fun onResume() {
        super.onResume()
        (activity as? MainActivity)?.card10BackgroundService?.checkBluetooth()
        ConnectionState.listener = this
    }

    override fun onPause() {
        super.onPause()
        ConnectionState.listener = null
    }

    private fun showConnectedView() {
        showConnectedContainer(true)

        val device = ConnectionService.device
        ConnectionService.addGattListener("main", this)
        if (firmwareVersion.isNullOrEmpty()) {
            ConnectionService.diService?.getFirmwareRevision()
        } else {
            setFirmwareVersion(firmwareVersion)
        }
        label_status.text = getString(R.string.main_label_connected, device?.name, device?.address)
        button_connect.text = getString(R.string.main_button_disconnect)
        button_connect.isEnabled = true
    }

    private fun showConnectingView() {
        showConnectedContainer(false)

        val device = ConnectionService.device
        label_status.text = getString(R.string.main_label_connecting, device?.name, device?.address)
        setFirmwareVersion(null)
        button_connect.text = getString(R.string.main_button_disconnect)
        button_connect.isEnabled = true
    }

    private fun showDisconnectedView() {
        showDisconnectedContainer()

        val device = ConnectionService.device
        label_status.text = getString(R.string.main_label_paired, device?.name, device?.address)
        setFirmwareVersion(null)
        button_connect.text = getString(R.string.main_button_connect)
        button_connect.isEnabled = true
        button_pair.text = getString(R.string.main_button_manage_pairings)
        button_pair.isEnabled = true
    }

    private fun showNotPairedView() {
        showDisconnectedContainer()

        label_status.text = getString(R.string.main_label_not_paired)
        setFirmwareVersion(null)
        button_connect.text = getString(R.string.main_button_connect)
        button_connect.isEnabled = false
        button_pair.text = getString(R.string.main_button_pair)
        button_pair.isEnabled = true
    }

    private fun showNoBluetoothView() {
        showDisconnectedContainer()

        label_status.text = getString(R.string.main_label_no_bluetooth)
        setFirmwareVersion(null)
        button_connect.text = getString(R.string.main_button_connect)
        button_connect.isEnabled = false
        button_pair.text = getString(R.string.main_button_pair)
        button_pair.isEnabled = false
    }

    private fun showConnectedContainer(buttonsEnabled : Boolean = false) {
        container_connected.visibility = View.VISIBLE
        container_disconnected.visibility = View.GONE

        button_hatchery.isEnabled = buttonsEnabled
        button_send.isEnabled = buttonsEnabled
        button_mood.isEnabled = buttonsEnabled
        button_beautiful.isEnabled = buttonsEnabled

        if(ConnectionService.card10EcgService == null) {
            button_ecg.visibility = View.GONE
        } else {
            button_ecg.visibility = View.VISIBLE
        }
        button_ecg.isEnabled = buttonsEnabled
        button_set_time.isEnabled = buttonsEnabled
    }

    private fun showDisconnectedContainer() {
        container_connected.visibility = View.GONE
        container_disconnected.visibility = View.VISIBLE
    }

    override fun onConnectionStateChanged(newState: ConnectionState) {
        activity?.runOnUiThread {
            when (newState) {
                ConnectionState.NOT_PAIRED -> showNotPairedView()
                ConnectionState.NOT_CONNECTED -> showDisconnectedView()
                ConnectionState.CONNECTING -> showConnectingView()
                ConnectionState.CONNECTED -> showConnectedView()
                ConnectionState.NO_BLUETOOTH -> showNoBluetoothView()
            }
        }
    }

    private fun setFirmwareVersion(version:String?)
    {
        firmwareVersion = version
        // Firmware versions before v1.18 report this string. Don't show it.
        if (!version.isNullOrEmpty() && version != "<git hash>") {
            label_firmware_version.text = getString(R.string.main_label_firmware_version, version)
        } else {
            label_firmware_version.text = ""
        }
    }

    override fun onCharacteristicRead(characteristic: BluetoothGattCharacteristic, status: Int) {

        when(characteristic.uuid) {
            FIMRWARE_REVISION_CHARACTERISTIC_UUID -> {
                activity?.runOnUiThread {
                    val version = characteristic.value.toString(Charsets.UTF_8)
                    setFirmwareVersion(version)

                    var major = 0
                    var minor = 0

                    val regex = """v(\d+)\.(\d+).*""".toRegex()
                    val matchResult = regex.find(version)
                    if (matchResult != null) {
                        val (major_s, minor_s) = matchResult!!.destructured
                        major = major_s.toInt()
                        minor = minor_s.toInt()
                    }

                    if (major < 1 || minor < 18) {
                        val d: AlertDialog = AlertDialog.Builder(context)
                            .setPositiveButton(android.R.string.ok, null)
                            .setMessage(R.string.main_update_message)
                            .create()
                        d.show()
                        // Make the textview clickable. Must be called after show()
                        (d.findViewById<View>(android.R.id.message) as TextView).movementMethod =
                            LinkMovementMethod.getInstance()
                    }
                }
            }
        }
    }
}
